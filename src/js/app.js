import $ from 'jquery';
import * as API from './Modules/Api';
import * as Credentials from './Modules/Credentials';
import * as Todolist from './Modules/Todolist';
import DomElements from "./Modules/DomElements";
import to from './Helper/to';

function initEvents() {
    updateView();
    onSubmitForm();
    onClickLoginButton();
    onClickRegisterButton();
    onKeypressAddItemInput();
    onClickRemoveItemInput();
    onClickAddTodolistButton();
    onClickRemoveTodolistButton();
    onClickLogoutButton();
    onClickEditItemButton();
}

function onSubmitForm() {
    DomElements.form.on('submit', async (event) => {
        event.preventDefault();

        let credentials = _getCredentialsFromForm();
        let register_response, login_response, err;

        // Login ou Register ?
        let action = DomElements.form.data('action');
        // Login =>
        if(action === 'login') {
            [err, login_response] = await to(API.login(credentials));
            if(!login_response) {
                _displayFormError("Identifiants incorrects");
            }
            else {
                DomElements.body.fadeOut('slow', () => {
                    _clearFormError();
                    Credentials.set(credentials);
                    Todolist.setToCache(login_response.data);
                    updateView();
                    Todolist.displayToDoList(login_response.data);
                    DomElements.body.fadeIn('slow');
                })
            }
        }
        // Register =>
        else {
            [err, register_response] = await to(API.register(credentials));
            if(!register_response) {
                _displayFormError("Une erreur est survenue");
            }
            else {
                _clearFormError();
                _changeFormAction('login');
            }
        }
    })
}

function onClickLoginButton() {
    DomElements.login_button.on('click', () => {
        let form_action = DomElements.form.attr('data-action');
        if(form_action !== 'login') {
            _changeFormAction('login');
        }
    });
}

function onKeypressAddItemInput() {
    $(document).on('keypress', '.add-item-input', async event => {
        if(event.keyCode === 13) {
            let item_value = $(event.currentTarget).val();
            let index_todolist = $(event.currentTarget).parent().index();
            await Todolist.addItemToList(index_todolist, item_value);
            $(event.currentTarget).parent().find('ul').append(`
                <li>
                    <span>${item_value}</span>
                    <img class="delete-item-list" src="img/close-black.svg" alt="croix noire">
                </li>
            `);

            $(event.currentTarget).val('');
        }
    })
}

function onClickRemoveItemInput() {
    $(document).on('click', '.delete-item-list', async event => {
        let index_item = $(event.currentTarget).parent().index();
        let index_todolist = $(event.currentTarget).parent().parent().parent().index();

        await Todolist.removeItemFromList(index_todolist, index_item);
        $(event.currentTarget).parent().remove();
    })
}

function onClickEditItemButton() {
    $(document).on('click', '.todolist ul li span', async event => {
        let old_value_item = $(event.currentTarget).text();
        let new_value_item = prompt('Modifier un item de la liste', old_value_item);
        let index_todolist = $(event.currentTarget).parent().parent().parent().index();
        let index_item = $(event.currentTarget).parent().index();

        await Todolist.editItemFromList(index_todolist, index_item, new_value_item);
        $(event.currentTarget).text(new_value_item);
    });
}

function onClickRegisterButton() {
    DomElements.register_button.on('click', () => {
        let form_action = DomElements.form.attr('data-action');
        if(form_action !== 'register') {
            _changeFormAction('register');
        }
    });
}

function onClickAddTodolistButton() {
    DomElements.add_todolist_button.on('click', async event => {
        let name_todolist = prompt('Name de la nouvelle Todolist');

        if(name_todolist.trim() !== '' && name_todolist.length > 0) {
            await Todolist.addTodolist(name_todolist);
            DomElements.todolist_container.append(`
                <div class="todolist">
                    <h3>${name_todolist}</h3>
                    <ul></ul>
                    <input class="add-item-input" type="text"placeholder="Ajouter un nouvel élément ...">
                    <span class="remove-todolist-button">Supprimer la Todolist</span>
                </div>
            `);
        }
    });
}

function onClickRemoveTodolistButton() {
    $(document).on('click','.remove-todolist-button', async event => {
        let index_todolist = $(event.currentTarget).parent().index();
        await Todolist.removeTodolist(index_todolist);

        $(event.currentTarget).parent().remove();
    })
}

function onClickLogoutButton() {
    DomElements.logout_header.on('click', event => {
        DomElements.body.fadeOut('slow', () => {
            Credentials.logout();
            updateView();
            DomElements.body.fadeIn('slow');
        });
    })
}

function updateView() {
    // Est ce que l'utilisateur est authentifié ?
    // Oui =>
    if(Credentials.isAuth()) {
        let credentials = Credentials.get();
        DomElements.guest.hide();
        DomElements.hello_header.text(`Bonjour ${credentials.login}`);
        Todolist.displayToDoList(Todolist.getFromCache());
        DomElements.auth.show();

    }
    else {
        DomElements.guest.show();
        DomElements.hello_header.text('');
        DomElements.auth.hide();
    }
}

function _getCredentialsFromForm() {
    let credentials = {};
    credentials.login = DomElements.login_input.val();
    credentials.password = DomElements.password_input.val();
    return credentials;
}

function _displayFormError(message) {
    DomElements.error_form.text(message);
}

function _clearFormError() {
    DomElements.error_form.text('');
}

function _changeFormAction(action) {
    DomElements.body.fadeOut('slow', () => {
        DomElements.form.attr('data-action', action);
        let title_form;
        action === 'login'
            ? title_form = 'Se Connecter'
            : title_form = 'Creer un compte';
        DomElements.form.find('h2').text(title_form);

        DomElements.body.fadeIn('slow');
    })
}


initEvents();
