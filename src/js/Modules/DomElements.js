import $ from 'jquery';
export default {
    // Body
    body: $('body'),

    // Header
    header: $('header'),
    login_button: $('.login-button'),
    register_button: $('.register-button'),
    hello_header: $('.hello-header'),
    logout_header: $('.logout-button'),

    // Form
    form: $('form'),
    login_input: $('input[name="login"]'),
    password_input: $('input[name="password"]'),
    error_form: $('.error-form'),

    // Auth and Guest class
    auth: $('.auth'),
    guest: $('.guest'),

    // todolist
    todolist_container: $('.todolist-container'),
    add_todolist_button: $('.add-todolist-button'),
    remove_todolist_button: $('.remove-todolist-button')
}