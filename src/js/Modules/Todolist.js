import to from "../Helper/to";
import DomElements from "./DomElements";
import * as API from "./Api";

export function displayToDoList(global_data) {
    let html='';

    global_data.todoListes.forEach((todolist_loop) => {

        html += _createTodolistElement(todolist_loop);
    });

    DomElements.todolist_container.html(html);
}

export function setToCache(data) {
    localStorage.setItem('todolist_data', JSON.stringify(data));
}

export function getFromCache() {
    return JSON.parse(localStorage.getItem('todolist_data'));
}

export async function addItemToList(index_todolist, item_value) {
    let global_data = getFromCache();
    global_data.todoListes[index_todolist].elements.push(item_value);
    setToCache(global_data);

    return await API.updateTodolist(global_data);

}

export async function addTodolist(name_todolist) {
    let global_data = getFromCache();
    global_data.todoListes.push({
        name: name_todolist,
        elements: [],
    });
    setToCache(global_data);

    return await API.updateTodolist(global_data);
}

export async function removeTodolist(index_todolist) {
    let global_data = getFromCache();
    global_data.todoListes = _removeElementFromArray(global_data.todoListes, index_todolist);
    setToCache(global_data);

    return await API.updateTodolist(global_data);
}

export async function editItemFromList(index_todolist, index_item, new_value_item) {
    let global_data = getFromCache();
    global_data.todoListes[index_todolist].elements[index_item] = new_value_item;
    setToCache(global_data);

    return await API.updateTodolist(global_data);
}

export async function removeItemFromList(index_todolist, index_item) {
    let global_data = getFromCache();
    global_data.todoListes[index_todolist].elements = _removeElementFromArray(global_data.todoListes[index_todolist].elements, index_item);
    setToCache(global_data);

    return await API.updateTodolist(global_data);
}

export function _createTodolistElement(single_data) {
    let html = `
        <div class="todolist">
            <h3>${single_data.name}</h3>
            <ul>
        `;
    single_data.elements.forEach(loop_element => {
        html += `
                <li>
                    <span>${loop_element}</span>
                    <img class="delete-item-list" src="img/close-black.svg" alt="croix noire">
                </li>`;
    });

    html += `
            </ul>
            <input class="add-item-input" type="text"placeholder="Ajouter un nouvel élément ...">
            <span class="remove-todolist-button">supprimer la Todolist</span>
        </div>
    `;

    return html;
}

export function _removeElementFromArray(array, index_element) {
    return array.slice(0, index_element).concat(array.slice(index_element + 1, array.length));
}