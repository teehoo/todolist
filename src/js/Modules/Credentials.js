export function get() {
    let credentials = {};
    credentials.login = localStorage.getItem('login');
    credentials.password = localStorage.getItem('password');

    return credentials;
}

export function set(credentials) {
    localStorage.setItem('login', credentials.login);
    localStorage.setItem('password', credentials.login);
}

export function isAuth() {
    let credentials = get();
    for (const key in credentials) {
        if(credentials.hasOwnProperty(key)) {
            if(!credentials[key]) {
                return false;
            }
        }
    }

    return true;
}

export function logout() {
    localStorage.clear();
}
