import axios from 'axios';
import to from '../Helper/to';
const API_ENDPOINT = 'http://92.222.69.104';

export function register(credentials) {
    return axios.get(`${API_ENDPOINT}/todo/create/${credentials.login}/${credentials.password}`);
}

export function login(credentials) {
    let config = {
        headers: {
            login: credentials.login,
            password: credentials.password
        }
    };
    return axios.get(`${API_ENDPOINT}/todo/listes`, config);
}

export function updateTodolist(body) {
    return axios.post(`${API_ENDPOINT}/todo/listes`, body);
}