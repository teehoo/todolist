const mix = require('laravel-mix');

mix.js('src/js/app.js', 'dist')
    .sass('src/style/app.scss', 'dist')
    .setPublicPath('dist');

mix.options({
    processCssUrls: false
});