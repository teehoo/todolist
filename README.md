# Todolist en JQuery et Ajax

Rendu TP noté JS


## Prérequis

Avoir **NodeJs** et **PHP**

## Installation

Une fois le projet téléchargé, il faut installer les dépendances Js avec npm

```
npm i
```

Ensuite se rendre dans le dossier dist et lancer un serveur PHP

```
cd ./dist
```
```
php -S localhost:3000
```

Une fois le serveur lancé, l'ouvrir dans le navigateur.

## Remarques

Pour éditer un item d'une liste, il faut cliquer sur le texte de l'item

Pour l'évaluation, les fichiers JS non mimifiés se trouvent dans le dossier **src**:
```
── src
│   ├── js
│   │   ├── Helper
│   │   │   └── to.js
│   │   ├── Modules
│   │   │   ├── Api.js
│   │   │   ├── Credentials.js
│   │   │   ├── DomElements.js
│   │   │   └── Todolist.js
│   │   └── app.js
```